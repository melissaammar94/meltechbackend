/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': 'CategoriesController.get',


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/
  //Categories APIs
  'GET /categories': 'CategoriesController.get',
  'GET /categories/:name': 'CategoriesController.getByName',
  'POST /categories': 'CategoriesController.create',
  'PUT /categories/:id': 'CategoriesController.update',
  'DELETE /categories/:id': 'CategoriesController.delete',

  //Devices APIs
  'GET /devices': 'DevicesController.get',
  'GET /devices/:name': 'DevicesController.getByName',
  'GET /devices/category/:name': 'DevicesController.getByCategory',
  'POST /devices': 'DevicesController.create',
  'PUT /devices/:id': 'DevicesController.update',
  'DELETE /devices/:id': 'DevicesController.delete',
};