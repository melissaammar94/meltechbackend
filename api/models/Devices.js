/**
 * Devices.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name:{
      type:'string', required: true, unique: true
    },
    brand:{
      type:'string', required: true
    },
    image:{
      type:'string', required: true
    },
    quantity:{
      type:'number', required: true
    },
    price:{
      type:'number', required: true
    },
    currency:{
      type:'string', required: true
    },
    category:{
      type:'string', required: true
    },
    specs:{
      type:'json', required: true
	  }
  },
};