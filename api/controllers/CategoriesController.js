/**
 * CategoriesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  get: function(req,res){
      Categories.find().then(function(categories){
          if(categories === null || categories.length === 0){
              return res.send({
                  'success': false,
                  'message': 'No categories found!'
              })
          }
          return res.send({
              'success': true,
              'message': 'Categories found!',
              'data': categories
          })
      })
      .catch(function(err){
          sails.log.debug(err);
          return res.send({
              'success': false,
              'message': 'Unable to fetch categories!'
          })              
    })
  },

  getByName: function(req,res){
    Categories.find({name: {'contains': req.param('name')}}).then(function(categories){
        if(categories === null || categories.length === 0){
            return res.send({
                'success': false,
                'message': 'No category found!'
            })
        }
        return res.send({
            'success': true,
            'message': 'Category found!',
            'data': categories
        })
    })
    .catch(function(err){
        sails.log.debug(err);
        return res.send({
            'success': false,
            'message': 'Unable to fetch category!'
        })              
    })
  },

  create: function(req,res){
      Categories.create(req.body).fetch().then(function(categories){
        if(categories === null || categories.length === 0){
            return res.send({
                'success': false,
                'message': 'Unable to create category!'
            })
        }
        return res.send({
            'success': true,
            'message': 'Category created successfully!',
            'data': categories
        })
    })
    .catch(function(err){
        sails.log.debug(err);
        return res.send({
            'success': false,
            'message': 'Unable to create category!'
        })              
    })
  },

  update: function(req,res){
    Categories.update(req.param('id'), req.body).fetch().then(function(categories){
        if(categories === null || categories.length === 0){
            return res.send({
                'success': false,
                'message': 'Unable to find category!'
            })
        }
        return res.send({
            'success': true,
            'message': 'Category updated successfully!',
            'data': categories
        })
    })
    .catch(function(err){
        sails.log.debug(err);
        return res.send({
            'success': false,
            'message': 'Unable to update category!'
        })              
    })
  },

  delete: function(req,res){
    Categories.destroy(req.param('id')).fetch().then(function(categories){
        if(categories === null || categories.length === 0){
            return res.send({
                'success': false,
                'message': 'Unable to find category!'
            })
        }
        return res.send({
            'success': true,
            'message': 'Category deleted successfully!',
            'data': categories
        })
    })
    .catch(function(err){
        sails.log.debug(err);
        return res.send({
            'success': false,
            'message': 'Unable to delete category!'
        })              
    })
  }
};