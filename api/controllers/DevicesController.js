/**
 * DevicesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    get: function(req,res){
        Devices.find().then(function(devices){
            if(devices === null || devices.length === 0){
                return res.send({
                    'success': false,
                    'message': 'No devices found!'
                })
            }
            return res.send({
                'success': true,
                'message': 'Devices found!',
                'data': devices
            })
        })
        .catch(function(err){
            sails.log.debug(err);
            return res.send({
                'success': false,
                'message': 'Unable to fetch devices!'
            })              
      })
    },
  
    getByName: function(req,res){
        Devices.find({name: {'contains': req.param('name')}}).then(function(devices){
            if(devices === null || devices.length === 0){
                return res.send({
                    'success': false,
                    'message': 'No device found!'
                })
            }
            return res.send({
                'success': true,
                'message': 'Device found!',
                'data': devices
          })
      })
      .catch(function(err){
          sails.log.debug(err);
          return res.send({
              'success': false,
              'message': 'Unable to fetch device!'
          })              
      })
    },

    getByCategory: function(req,res){
        Devices.find({category: {'contains': req.param('name')}}).then(function(devices){
            if(devices === null || devices.length === 0){
                return res.send({
                    'success': false,
                    'message': 'No devices found!'
                })
            }
            return res.send({
                'success': true,
                'message': 'Devices found!',
                'data': devices
          })
      })
      .catch(function(err){
          sails.log.debug(err);
          return res.send({
              'success': false,
              'message': 'Unable to fetch devices!'
          })              
      })
    },

    create: function(req,res){
        Devices.create(req.body).fetch().then(function(devices){
          if(devices === null || devices.length === 0){
              return res.send({
                  'success': false,
                  'message': 'Unable to create device!'
              })
          }
          return res.send({
              'success': true,
              'message': 'Device created successfully!',
              'data': devices
          })
      })
      .catch(function(err){
          sails.log.debug(err);
          return res.send({
              'success': false,
              'message': 'Unable to create device!'
          })              
      })
    },
  
    update: function(req,res){
        Devices.update(req.param('id'), req.body).fetch().then(function(devices){
          if(devices === null || devices.length === 0){
              return res.send({
                  'success': false,
                  'message': 'Unable to find device!'
              })
          }
          return res.send({
              'success': true,
              'message': 'Device updated successfully!',
              'data': devices
          })
      })
      .catch(function(err){
          sails.log.debug(err);
          return res.send({
              'success': false,
              'message': 'Unable to update device!'
          })              
      })
    },
  
    delete: function(req,res){
        Devices.destroy(req.param('id')).fetch().then(function(devices){
            if(devices === null || devices.length === 0){
                return res.send({
                    'success': false,
                    'message': 'Unable to find device!'
                })
            }
            return res.send({
                'success': true,
                'message': 'Device deleted successfully!',
                'data': devices
            })
        })
        .catch(function(err){
            sails.log.debug(err);
            return res.send({
                'success': false,
                'message': 'Unable to delete device!'
            })              
        })
    }
};